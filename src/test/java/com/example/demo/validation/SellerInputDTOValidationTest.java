package com.example.demo.validation;

import com.example.demo.dto.SellerInputDTO;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SellerInputDTOValidationTest {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    private SellerInputDTO sellerInputDTO;

    @Test
    void validationCorrectTest() {
        sellerInputDTO = new SellerInputDTO();
        sellerInputDTO.setTitle("Sony");
        sellerInputDTO.setCountry("Russia");
        sellerInputDTO.setCity("Moscow");
        sellerInputDTO.setPhoneNumber("12345678901");

        Set<ConstraintViolation<SellerInputDTO>> violations = validator.validate(sellerInputDTO);

        assertEquals(violations.size(), 0);
    }

    @Test
    void validationFailTest() {
        sellerInputDTO = new SellerInputDTO();
        sellerInputDTO.setTitle("");
        sellerInputDTO.setCountry("");
        sellerInputDTO.setCity("");
        sellerInputDTO.setPhoneNumber("12345ko");

        Set<ConstraintViolation<SellerInputDTO>> violations = validator.validate(sellerInputDTO);

        assertEquals(violations.size(), 4);
    }

}