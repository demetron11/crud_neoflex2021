package com.example.demo.validation;

import com.example.demo.dto.CustomerInputDTO;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerInputDTOValidationTest {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    private CustomerInputDTO customerInputDTO;

    @Test
    void validationCorrectTest() {
        customerInputDTO = new CustomerInputDTO();
        customerInputDTO.setName("Oleg");
        customerInputDTO.setPhoneNumber("12345678901");
        customerInputDTO.setBirthday(LocalDate.of(1990,  7, 12));
        customerInputDTO.setAddress("Moscow");

        Set<ConstraintViolation<CustomerInputDTO>> violations = validator.validate(customerInputDTO);

        assertEquals(violations.size(), 0);
    }

    @Test
    void validationFailTest() {
        customerInputDTO = new CustomerInputDTO();
        customerInputDTO.setName("");
        customerInputDTO.setPhoneNumber("123678901");
        customerInputDTO.setBirthday(LocalDate.of(1564,  7, 12));
        customerInputDTO.setAddress("");

        Set<ConstraintViolation<CustomerInputDTO>> violations = validator.validate(customerInputDTO);

        assertEquals(violations.size(), 4);
    }

}