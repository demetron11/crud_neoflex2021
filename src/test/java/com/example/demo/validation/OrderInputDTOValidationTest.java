package com.example.demo.validation;

import com.example.demo.dto.OrderInputDTO;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderInputDTOValidationTest {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    private OrderInputDTO orderInputDTO;

    @Test
    void validationCorrectTest() {
        orderInputDTO = new OrderInputDTO();
        orderInputDTO.setOpenDate(LocalDate.of(1990,  7, 12));
        orderInputDTO.setCloseDate(LocalDate.of(1990,  7, 12));
        orderInputDTO.setProductIds(List.of(1, 2, 5));

        Set<ConstraintViolation<OrderInputDTO>> violations = validator.validate(orderInputDTO);

        assertEquals(violations.size(), 0);
    }

    @Test
    void validationFailTest() {
        orderInputDTO = new OrderInputDTO();
        orderInputDTO.setOpenDate(LocalDate.of(1308,  7, 12));
        orderInputDTO.setCloseDate(LocalDate.of(980,  7, 12));
        orderInputDTO.setProductIds(List.of());

        Set<ConstraintViolation<OrderInputDTO>> violations = validator.validate(orderInputDTO);

        assertEquals(violations.size(), 3);
    }

}