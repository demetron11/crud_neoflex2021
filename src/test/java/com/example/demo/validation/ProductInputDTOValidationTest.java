package com.example.demo.validation;

import com.example.demo.dto.ProductInputDTO;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductInputDTOValidationTest {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    private ProductInputDTO productInputDTO;

    @Test
    void validationCorrectTest() {
        productInputDTO = new ProductInputDTO();
        productInputDTO.setTitle("Sony-100");
        productInputDTO.setPrice(BigDecimal.valueOf(4500.00));

        Set<ConstraintViolation<ProductInputDTO>> violations = validator.validate(productInputDTO);

        assertEquals(violations.size(), 0);
    }

    @Test
    void validationFailTest() {
        productInputDTO = new ProductInputDTO();
        productInputDTO.setTitle("");
        productInputDTO.setPrice(BigDecimal.valueOf(-2300.00));

        Set<ConstraintViolation<ProductInputDTO>> violations = validator.validate(productInputDTO);

        assertEquals(violations.size(), 2);
    }

}