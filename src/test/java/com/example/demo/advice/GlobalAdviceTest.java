package com.example.demo.advice;

import com.example.demo.controller.CustomerController;
import com.example.demo.exception.ObjectNotFoundException;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

@ExtendWith(MockitoExtension.class)
class GlobalAdviceTest {

    private MockMvc mockMvc;

    @Mock
    private CustomerController customerController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(customerController)
                .setControllerAdvice(new GlobalAdvice())
                .build();
    }

    @Test
    @SneakyThrows
    void handleNotFoundExceptionTest() {
        Mockito.when(customerController.deleteCustomerById(74))
                .thenThrow(new ObjectNotFoundException());

        mockMvc.perform(delete("/customers/74")).andExpect(result -> new ResponseEntity<>("Объект с данным ID не найден", HttpStatus.valueOf(400)));
    }

}