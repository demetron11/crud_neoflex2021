package com.example.demo.service.impl;

import com.example.demo.entity.Order;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderServiceImpl orderServiceImpl;

    private Order mockOrder;

    @BeforeEach
    void setUp() {
        mockOrder = new Order();
    }

    @Test
    void getOrderByIdIfPresent() {
        when(orderRepository.findById(74)).thenReturn(Optional.of(mockOrder));

        Order order = orderServiceImpl.getOrderById(74);
        assertEquals(order, mockOrder);
    }

    @Test
    void getOrderByIdIfAbsent() {
        when(orderRepository.findById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class, () -> orderServiceImpl.getOrderById(74));
    }

    @Test
    void saveOrder() {
        mockOrder.setId(74);
        when(orderRepository.save(mockOrder)).thenReturn(mockOrder);

        Integer id = orderServiceImpl.saveOrder(mockOrder);
        assertEquals(id, 74);
    }

    @Test
    void updateOrderIfPresent() {
        when(orderRepository.existsById(74)).thenReturn(true);

        orderServiceImpl.updateOrder(74, mockOrder);
        assertEquals(mockOrder.getId(), 74);
    }

    @Test
    void updateOrderIfAbsent() {
        when(orderRepository.existsById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> orderServiceImpl.updateOrder(74, mockOrder));
    }

    @Test
    void deleteOrderByIdIfPresent() {
        when(orderRepository.existsById(74)).thenReturn(true);

        orderServiceImpl.deleteOrderById(74);
        verify(orderRepository, times(1)).deleteById(74);

    }

    @Test
    void deleteOrderByIdIfAbsent() {
        when(orderRepository.existsById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> orderServiceImpl.deleteOrderById(74));
    }

    @Test
    void getAllOrders() {
        when(orderRepository.findAll()).thenReturn(Collections.singletonList(mockOrder));

        List<Order> allOrders = orderServiceImpl.getAllOrders();
        assertEquals(allOrders.size(), 1);
    }

}