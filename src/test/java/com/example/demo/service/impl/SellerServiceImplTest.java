package com.example.demo.service.impl;

import com.example.demo.entity.Seller;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.SellerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SellerServiceImplTest {

    @Mock
    private SellerRepository sellerRepository;

    @InjectMocks
    private SellerServiceImpl sellerServiceImpl;

    private Seller mockSeller;

    @BeforeEach
    void setUp() {
        mockSeller = new Seller();
    }

    @Test
    void getSellerByIdIfPresent() {
        when(sellerRepository.findById(74)).thenReturn(Optional.of(mockSeller));

        Seller seller = sellerServiceImpl.getSellerById(74);
        assertEquals(seller, mockSeller);
    }

    @Test
    void getSellerByIdIfAbsent() {
        when(sellerRepository.findById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class, () -> sellerServiceImpl.getSellerById(74));
    }

    @Test
    void saveSeller() {
        mockSeller.setId(74);
        when(sellerRepository.save(mockSeller)).thenReturn(mockSeller);

        Integer id = sellerServiceImpl.saveSeller(mockSeller);
        assertEquals(id, 74);
    }

    @Test
    void updateSellerIfPresent() {
        when(sellerRepository.existsById(74)).thenReturn(true);

        sellerServiceImpl.updateSeller(74, mockSeller);
        assertEquals(mockSeller.getId(), 74);
    }

    @Test
    void updateSellerIfAbsent() {
        when(sellerRepository.existsById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> sellerServiceImpl.updateSeller(74, mockSeller));
    }

    @Test
    void deleteSellerByIdIfPresent() {
        when(sellerRepository.existsById(74)).thenReturn(true);

        sellerServiceImpl.deleteSellerById(74);
        verify(sellerRepository, times(1)).deleteById(74);

    }

    @Test
    void deleteSellerByIdIfAbsent() {
        when(sellerRepository.existsById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> sellerServiceImpl.deleteSellerById(74));
    }

    @Test
    void getAllSellers() {
        when(sellerRepository.findAll()).thenReturn(Collections.singletonList(mockSeller));

        List<Seller> allSellers = sellerServiceImpl.getAllSellers();
        assertEquals(allSellers.size(), 1);
    }
}