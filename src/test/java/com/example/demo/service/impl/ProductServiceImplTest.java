package com.example.demo.service.impl;

import com.example.demo.entity.Product;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productServiceImpl;

    private Product mockProduct;

    @BeforeEach
    void setUp() {
        mockProduct = new Product();
    }

    @Test
    void getProductByIdIfPresent() {
        when(productRepository.findById(74)).thenReturn(Optional.of(mockProduct));

        Product product = productServiceImpl.getProductById(74);
        assertEquals(product, mockProduct);
    }

    @Test
    void getProductByIdIfAbsent() {
        when(productRepository.findById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class, () -> productServiceImpl.getProductById(74));
    }

    @Test
    void saveProduct() {
        mockProduct.setId(74);
        when(productRepository.save(mockProduct)).thenReturn(mockProduct);

        Integer id = productServiceImpl.saveProduct(mockProduct);
        assertEquals(id, 74);
    }

    @Test
    void updateProductIfPresent() {
        when(productRepository.existsById(74)).thenReturn(true);

        productServiceImpl.updateProduct(74, mockProduct);
        assertEquals(mockProduct.getId(), 74);
    }

    @Test
    void updateProductIfAbsent() {
        when(productRepository.existsById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> productServiceImpl.updateProduct(74, mockProduct));
    }

    @Test
    void deleteProductByIdIfPresent() {
        when(productRepository.existsById(74)).thenReturn(true);

        productServiceImpl.deleteProductById(74);
        verify(productRepository, times(1)).deleteById(74);

    }

    @Test
    void deleteProductByIdIfAbsent() {
        when(productRepository.existsById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> productServiceImpl.deleteProductById(74));
    }

    @Test
    void getAllProducts() {
        when(productRepository.findAll()).thenReturn(Collections.singletonList(mockProduct));

        List<Product> allProducts = productServiceImpl.getAllProducts();
        assertEquals(allProducts.size(), 1);
    }

    @Test
    void getProductsFromList() {
        List<Integer> integerList = Collections.singletonList(74);
        when(productRepository.findAllById(integerList))
                .thenReturn(Collections.singletonList(mockProduct));

        List<Product> allProducts = productServiceImpl.getListProducts(integerList);
        assertEquals(allProducts.size(), 1);
    }
}