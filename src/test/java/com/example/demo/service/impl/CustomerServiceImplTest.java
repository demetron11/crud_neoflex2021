package com.example.demo.service.impl;

import com.example.demo.entity.Customer;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerServiceImpl customerServiceImpl;

    private Customer mockCustomer;

    @BeforeEach
    void setUp() {
        mockCustomer = new Customer();
    }

    @Test
    void getCustomerByIdIfPresent() {
        when(customerRepository.findById(74)).thenReturn(Optional.of(mockCustomer));

        Customer customer = customerServiceImpl.getCustomerById(74);
        assertEquals(customer, mockCustomer);
    }

    @Test
    void getCustomerByIdIfAbsent() {
        when(customerRepository.findById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class, () -> customerServiceImpl.getCustomerById(74));
    }

    @Test
    void saveCustomer() {
        mockCustomer.setId(74);
        when(customerRepository.save(mockCustomer)).thenReturn(mockCustomer);

        Integer id = customerServiceImpl.saveCustomer(mockCustomer);
        assertEquals(id, 74);
    }

    @Test
    void updateCustomerIfPresent() {
        when(customerRepository.existsById(74)).thenReturn(true);

        customerServiceImpl.updateCustomer(74, mockCustomer);
        assertEquals(mockCustomer.getId(), 74);
    }

    @Test
    void updateCustomerIfAbsent() {
        when(customerRepository.existsById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> customerServiceImpl.updateCustomer(74, mockCustomer));
    }

    @Test
    void deleteCustomerByIdIfPresent() {
        when(customerRepository.existsById(74)).thenReturn(true);

        customerServiceImpl.deleteCustomerById(74);
        verify(customerRepository, times(1)).deleteById(74);

    }

    @Test
    void deleteCustomerByIdIfAbsent() {
        when(customerRepository.existsById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> customerServiceImpl.deleteCustomerById(74));
    }

    @Test
    void getAllCustomers() {
        when(customerRepository.findAll()).thenReturn(Collections.singletonList(mockCustomer));

        List<Customer> allCustomers = customerServiceImpl.getAllCustomers();
        assertEquals(allCustomers.size(), 1);
    }
}