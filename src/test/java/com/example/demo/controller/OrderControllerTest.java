package com.example.demo.controller;

import com.example.demo.dto.OrderInputDTO;
import com.example.demo.dto.OrderOutputDTO;
import com.example.demo.entity.Order;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderControllerTest {

    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderController orderController;

    @Spy
    private OrderMapper orderMapper;

    private OrderOutputDTO mockOrderOutputDTO;

    private OrderInputDTO mockOrderInputDTO;

    private Order mockOrder;

    @BeforeEach
    void setUp() {
        mockOrderInputDTO = new OrderInputDTO();
        mockOrder = orderMapper.mapToOrder(mockOrderInputDTO);
        mockOrderOutputDTO = orderMapper.mapToDTO(mockOrder);
    }

    @Test
    void getAllOrders() {
        when(orderService.getAllOrders()).thenReturn(Collections.singletonList(mockOrder));

        assertEquals(orderController.getAllOrders(),
                new ResponseEntity<>(Collections.singletonList(mockOrderOutputDTO), HttpStatus.OK));
    }

    @Test
    void getOrderByIdIfPresent() {
        when(orderService.getOrderById(74)).thenReturn(mockOrder);

        assertEquals(orderController.getOrderById(74),
                new ResponseEntity<>(mockOrderOutputDTO, HttpStatus.OK));
    }

    @Test
    void getOrderByIdIfAbsent() {
        when(orderService.getOrderById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> orderController.getOrderById(74));
    }

    @Test
    void createOrder() {
        when(orderService.saveOrder(mockOrder)).thenReturn(74);

        assertEquals(orderController.createOrder(mockOrderInputDTO),
                new ResponseEntity<>(74, HttpStatus.CREATED));
    }

    @Test
    void deleteOrderByIdIfPresent() {
        doNothing().when(orderService).deleteOrderById(74);

        assertEquals(orderController.deleteOrderById(74),
                new ResponseEntity<>(HttpStatus.NO_CONTENT));
        verify(orderService, times(1)).deleteOrderById(74);
    }

    @Test
    void deleteOrderByIdIfAbsent() {
        doThrow(new ObjectNotFoundException()).when(orderService).deleteOrderById(74);

        assertThrows(ObjectNotFoundException.class,
                () -> orderController.deleteOrderById(74));
    }

    @Test
    void updateOrderIfPresent() {
        doNothing().when(orderService).updateOrder(74, mockOrder);

        assertEquals(orderController.updateOrder(74, mockOrderInputDTO),
                new ResponseEntity<>(HttpStatus.NO_CONTENT));
        verify(orderService, times(1)).updateOrder(74, mockOrder);
    }

    @Test
    void updateOrderIfAbsent() {
        doThrow(new ObjectNotFoundException()).when(orderService).updateOrder(74, mockOrder);

        assertThrows(ObjectNotFoundException.class,
                () -> orderController.updateOrder(74, mockOrderInputDTO));

    }
}