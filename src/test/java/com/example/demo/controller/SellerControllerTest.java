package com.example.demo.controller;

import com.example.demo.dto.SellerInputDTO;
import com.example.demo.dto.SellerOutputDTO;
import com.example.demo.entity.Seller;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.mapper.SellerMapper;
import com.example.demo.service.SellerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SellerControllerTest {

    @Mock
    private SellerService sellerService;

    @InjectMocks
    private SellerController sellerController;

    @Spy
    private SellerMapper sellerMapper;

    private SellerOutputDTO mockSellerOutputDTO;

    private SellerInputDTO mockSellerInputDTO;

    private Seller mockSeller;

    @BeforeEach
    void setUp() {
        mockSellerInputDTO = new SellerInputDTO();
        mockSeller = sellerMapper.mapToSeller(mockSellerInputDTO);
        mockSellerOutputDTO = sellerMapper.mapToDTO(mockSeller);
    }

    @Test
    void getAllSellers() {
        when(sellerService.getAllSellers()).thenReturn(Collections.singletonList(mockSeller));

        assertEquals(sellerController.getAllSellers(),
                new ResponseEntity<>(Collections.singletonList(mockSellerOutputDTO), HttpStatus.OK));
    }

    @Test
    void getSellerByIdIfPresent() {
        when(sellerService.getSellerById(74)).thenReturn(mockSeller);

        assertEquals(sellerController.getSellerById(74),
                new ResponseEntity<>(mockSellerOutputDTO, HttpStatus.OK));
    }

    @Test
    void getSellerByIdIfAbsent() {
        when(sellerService.getSellerById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> sellerController.getSellerById(74));
    }

    @Test
    void createSeller() {
        when(sellerService.saveSeller(mockSeller)).thenReturn(74);

        assertEquals(sellerController.createSeller(mockSellerInputDTO),
                new ResponseEntity<>(74, HttpStatus.CREATED));
    }

    @Test
    void deleteSellerByIdIfPresent() {
        doNothing().when(sellerService).deleteSellerById(74);

        assertEquals(sellerController.deleteSellerById(74),
                new ResponseEntity<>(HttpStatus.NO_CONTENT));
        verify(sellerService, times(1)).deleteSellerById(74);
    }

    @Test
    void deleteSellerByIdIfAbsent() {
        doThrow(new ObjectNotFoundException()).when(sellerService).deleteSellerById(74);

        assertThrows(ObjectNotFoundException.class,
                () -> sellerController.deleteSellerById(74));
    }

    @Test
    void updateSellerIfPresent() {
        doNothing().when(sellerService).updateSeller(74, mockSeller);

        assertEquals(sellerController.updateSeller(74, mockSellerInputDTO),
                new ResponseEntity<>(HttpStatus.NO_CONTENT));
        verify(sellerService, times(1)).updateSeller(74, mockSeller);
    }

    @Test
    void updateSellerIfAbsent() {
        doThrow(new ObjectNotFoundException()).when(sellerService).updateSeller(74, mockSeller);

        assertThrows(ObjectNotFoundException.class,
                () -> sellerController.updateSeller(74, mockSellerInputDTO));

    }
}