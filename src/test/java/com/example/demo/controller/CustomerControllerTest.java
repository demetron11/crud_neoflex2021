package com.example.demo.controller;

import com.example.demo.dto.CustomerInputDTO;
import com.example.demo.dto.CustomerOutputDTO;
import com.example.demo.entity.Customer;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.mapper.CustomerMapper;
import com.example.demo.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;

    @Spy
    private CustomerMapper customerMapper;

    private CustomerOutputDTO mockCustomerOutputDTO;

    private CustomerInputDTO mockCustomerInputDTO;

    private Customer mockCustomer;

    @BeforeEach
    void setUp() {
        mockCustomerInputDTO = new CustomerInputDTO();
        mockCustomer = customerMapper.mapToCustomer(mockCustomerInputDTO);
        mockCustomerOutputDTO = customerMapper.mapToDTO(mockCustomer);
    }

    @Test
    void getAllCustomers() {
        when(customerService.getAllCustomers()).thenReturn(Collections.singletonList(mockCustomer));

        assertEquals(customerController.getAllCustomers(),
                new ResponseEntity<>(Collections.singletonList(mockCustomerOutputDTO), HttpStatus.OK));
    }

    @Test
    void getCustomerByIdIfPresent() {
        when(customerService.getCustomerById(74)).thenReturn(mockCustomer);

        assertEquals(customerController.getCustomerById(74),
                new ResponseEntity<>(mockCustomerOutputDTO, HttpStatus.OK));
    }

    @Test
    void getCustomerByIdIfAbsent() {
        when(customerService.getCustomerById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> customerController.getCustomerById(74));
    }

    @Test
    void createCustomer() {
        when(customerService.saveCustomer(mockCustomer)).thenReturn(74);

        assertEquals(customerController.createCustomer(mockCustomerInputDTO),
                new ResponseEntity<>(74, HttpStatus.CREATED));
    }

    @Test
    void deleteCustomerByIdIfPresent() {
        doNothing().when(customerService).deleteCustomerById(74);

        assertEquals(customerController.deleteCustomerById(74),
                new ResponseEntity<>(HttpStatus.NO_CONTENT));
        verify(customerService, times(1)).deleteCustomerById(74);
    }

    @Test
    void deleteCustomerByIdIfAbsent() {
        doThrow(new ObjectNotFoundException()).when(customerService).deleteCustomerById(74);

        assertThrows(ObjectNotFoundException.class,
                () -> customerController.deleteCustomerById(74));
    }

    @Test
    void updateCustomerIfPresent() {
        doNothing().when(customerService).updateCustomer(74, mockCustomer);

        assertEquals(customerController.updateCustomer(74, mockCustomerInputDTO),
                new ResponseEntity<>(HttpStatus.NO_CONTENT));
        verify(customerService, times(1)).updateCustomer(74, mockCustomer);
    }

    @Test
    void updateCustomerIfAbsent() {
        doThrow(new ObjectNotFoundException()).when(customerService).updateCustomer(74, mockCustomer);

        assertThrows(ObjectNotFoundException.class,
                () -> customerController.updateCustomer(74, mockCustomerInputDTO));

    }
}