package com.example.demo.controller;

import com.example.demo.dto.ProductInputDTO;
import com.example.demo.dto.ProductOutputDTO;
import com.example.demo.entity.Product;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    @Spy
    private ProductMapper productMapper;

    private ProductOutputDTO mockProductOutputDTO;

    private ProductInputDTO mockProductInputDTO;

    private Product mockProduct;

    @BeforeEach
    void setUp() {
        mockProductInputDTO = new ProductInputDTO();
        mockProduct = productMapper.mapToProduct(mockProductInputDTO);
        mockProductOutputDTO = productMapper.mapToDTO(mockProduct);
    }

    @Test
    void getAllProducts() {
        when(productService.getAllProducts()).thenReturn(Collections.singletonList(mockProduct));

        assertEquals(productController.getAllProducts(),
                new ResponseEntity<>(Collections.singletonList(mockProductOutputDTO), HttpStatus.OK));
    }

    @Test
    void getProductByIdIfPresent() {
        when(productService.getProductById(74)).thenReturn(mockProduct);

        assertEquals(productController.getProductById(74),
                new ResponseEntity<>(mockProductOutputDTO, HttpStatus.OK));
    }

    @Test
    void getProductByIdIfAbsent() {
        when(productService.getProductById(74)).thenThrow(new ObjectNotFoundException());

        assertThrows(ObjectNotFoundException.class,
                () -> productController.getProductById(74));
    }

    @Test
    void createProduct() {
        when(productService.saveProduct(mockProduct)).thenReturn(74);

        assertEquals(productController.createProduct(mockProductInputDTO),
                new ResponseEntity<>(74, HttpStatus.CREATED));
    }

    @Test
    void deleteProductByIdIfPresent() {
        doNothing().when(productService).deleteProductById(74);

        assertEquals(productController.deleteProductById(74),
                new ResponseEntity<>(HttpStatus.NO_CONTENT));
        verify(productService, times(1)).deleteProductById(74);
    }

    @Test
    void deleteProductByIdIfAbsent() {
        doThrow(new ObjectNotFoundException()).when(productService).deleteProductById(74);

        assertThrows(ObjectNotFoundException.class,
                () -> productController.deleteProductById(74));
    }

    @Test
    void updateProductIfPresent() {
        doNothing().when(productService).updateProduct(74, mockProduct);

        assertEquals(productController.updateProduct(74, mockProductInputDTO),
                new ResponseEntity<>(HttpStatus.NO_CONTENT));
        verify(productService, times(1)).updateProduct(74, mockProduct);
    }

    @Test
    void updateProductIfAbsent() {
        doThrow(new ObjectNotFoundException()).when(productService).updateProduct(74, mockProduct);

        assertThrows(ObjectNotFoundException.class,
                () -> productController.updateProduct(74, mockProductInputDTO));

    }
}