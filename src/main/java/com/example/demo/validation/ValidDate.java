package com.example.demo.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = DateValidator.class)
public @interface ValidDate {

    String message() default "Ошибка ввода даты";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
