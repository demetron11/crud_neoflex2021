package com.example.demo.exception;

public class ObjectNotFoundException extends RuntimeException {

    public ObjectNotFoundException() {
        super("Объект с данным ID не найден");
    }
}
