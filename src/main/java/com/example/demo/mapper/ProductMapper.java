package com.example.demo.mapper;

import com.example.demo.dto.ProductInputDTO;
import com.example.demo.dto.ProductOutputDTO;
import com.example.demo.entity.Product;
import com.example.demo.entity.Seller;
import com.example.demo.service.SellerService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class ProductMapper {

    @Autowired
    private SellerService sellerService;

    public abstract ProductOutputDTO mapToDTO(Product product);

    @Mapping(target = "seller", expression = "java(getSeller(productInputDTO.getSellerId()))")
    public abstract Product mapToProduct(ProductInputDTO productInputDTO);

    Seller getSeller(Integer id) {return sellerService.getSellerById(id);}
}
