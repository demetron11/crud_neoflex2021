package com.example.demo.mapper;

import com.example.demo.dto.CustomerOutputDTO;
import com.example.demo.dto.CustomerInputDTO;
import com.example.demo.entity.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerOutputDTO mapToDTO(Customer customer);

    Customer mapToCustomer(CustomerInputDTO customerInputDTO);
}
