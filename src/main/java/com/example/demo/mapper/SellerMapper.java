package com.example.demo.mapper;

import com.example.demo.dto.SellerInputDTO;
import com.example.demo.dto.SellerOutputDTO;
import com.example.demo.entity.Seller;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SellerMapper {

    SellerOutputDTO mapToDTO(Seller seller);

    Seller mapToSeller(SellerInputDTO sellerInputDTO);
}
