package com.example.demo.mapper;

import com.example.demo.dto.OrderInputDTO;
import com.example.demo.dto.OrderOutputDTO;
import com.example.demo.entity.Customer;
import com.example.demo.entity.Order;
import com.example.demo.entity.Product;
import com.example.demo.service.CustomerService;
import com.example.demo.service.ProductService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class OrderMapper {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    public abstract OrderOutputDTO mapToDTO(Order order);

    @Mapping(target = "customer", expression = "java(getCustomer(orderInputDTO.getCustomerId()))")
    @Mapping(target = "products", expression = "java(getListProducts(orderInputDTO.getProductIds()))")
    public abstract Order mapToOrder(OrderInputDTO orderInputDTO);

    Customer getCustomer(Integer id) {
        return customerService.getCustomerById(id);
    }

    List<Product> getListProducts(List<Integer> productIds) {
        return productService.getListProducts(productIds);
    }
}
