package com.example.demo.service;

import com.example.demo.entity.Customer;

import java.util.List;

public interface CustomerService {

    Customer getCustomerById(Integer id);

    Integer saveCustomer(Customer customer);

    void updateCustomer(Integer id, Customer customer);

    void deleteCustomerById(Integer id);

    List<Customer> getAllCustomers();
}
