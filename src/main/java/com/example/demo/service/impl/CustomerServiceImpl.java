package com.example.demo.service.impl;

import com.example.demo.entity.Customer;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.findById(id).orElseThrow(ObjectNotFoundException::new);}

    @Override
    public Integer saveCustomer(Customer customer) {
        return customerRepository.save(customer).getId();}

    @Override
    public void updateCustomer(Integer id, Customer customer) {
        if (!customerRepository.existsById(id)) throw new ObjectNotFoundException();
        customer.setId(id);
        customerRepository.save(customer);
    }

    @Override
    public void deleteCustomerById(Integer id) {
        if (!customerRepository.existsById(id)) throw new ObjectNotFoundException();
        customerRepository.deleteById(id);}

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }
}
