package com.example.demo.service.impl;

import com.example.demo.entity.Order;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.OrderRepository;
import com.example.demo.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Override
    public Order getOrderById(Integer id) {
        return orderRepository.findById(id).orElseThrow(ObjectNotFoundException::new);}

    @Override
    public Integer saveOrder(Order order) {
        return orderRepository.save(order).getId();}

    @Override
    public void updateOrder(Integer id, Order order) {
        if (!orderRepository.existsById(id)) throw new ObjectNotFoundException();
        order.setId(id);
        orderRepository.save(order);
    }

    @Override
    public void deleteOrderById(Integer id) {
        if (!orderRepository.existsById(id)) throw new ObjectNotFoundException();
        orderRepository.deleteById(id);}

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
}
