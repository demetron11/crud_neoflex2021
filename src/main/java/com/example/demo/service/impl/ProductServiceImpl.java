package com.example.demo.service.impl;

import com.example.demo.entity.Product;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public Product getProductById(Integer id) {
        return productRepository.findById(id).orElseThrow(ObjectNotFoundException::new);}

    @Override
    public Integer saveProduct(Product product) {
        return productRepository.save(product).getId();}

    @Override
    public void updateProduct(Integer id, Product product) {
        if (!productRepository.existsById(id)) throw new ObjectNotFoundException();
        product.setId(id);
        productRepository.save(product);
    }

    @Override
    public void deleteProductById(Integer id) {
        if (!productRepository.existsById(id)) throw new ObjectNotFoundException();
        productRepository.deleteById(id);}

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> getListProducts(List<Integer> productIds) {
        return productRepository.findAllById(productIds);
    }
}
