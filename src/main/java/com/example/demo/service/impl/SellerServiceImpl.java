package com.example.demo.service.impl;

import com.example.demo.entity.Seller;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.SellerRepository;
import com.example.demo.service.SellerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class SellerServiceImpl implements SellerService {

    private final SellerRepository sellerRepository;

    @Override
    public Seller getSellerById(Integer id) {
        return sellerRepository.findById(id).orElseThrow(ObjectNotFoundException::new);}

    @Override
    public Integer saveSeller(Seller seller) {
        return sellerRepository.save(seller).getId();}

    @Override
    public void updateSeller(Integer id, Seller seller) {
        if (!sellerRepository.existsById(id)) throw new ObjectNotFoundException();
        seller.setId(id);
        sellerRepository.save(seller);
    }

    @Override
    public void deleteSellerById(Integer id) {
        if (!sellerRepository.existsById(id)) throw new ObjectNotFoundException();
        sellerRepository.deleteById(id);}

    @Override
    public List<Seller> getAllSellers() {
        return sellerRepository.findAll();
    }
}
