package com.example.demo.service;

import com.example.demo.entity.Order;

import java.util.List;

public interface OrderService {

    Order getOrderById(Integer id);

    Integer saveOrder(Order order);

    void updateOrder(Integer id, Order order);

    void deleteOrderById(Integer id);

    List<Order> getAllOrders();
}
