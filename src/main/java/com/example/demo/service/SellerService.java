package com.example.demo.service;

import com.example.demo.entity.Seller;

import java.util.List;

public interface SellerService {

    Seller getSellerById(Integer id);

    Integer saveSeller(Seller seller);

    void updateSeller(Integer id, Seller seller);

    void deleteSellerById(Integer id);

    List<Seller> getAllSellers();
}
