package com.example.demo.service;

import com.example.demo.entity.Product;

import java.util.List;

public interface ProductService {

    Product getProductById(Integer id);

    Integer saveProduct(Product product);

    void updateProduct(Integer id, Product product);

    void deleteProductById(Integer id);

    List<Product> getAllProducts();

    List<Product> getListProducts(List<Integer> productIds);
}