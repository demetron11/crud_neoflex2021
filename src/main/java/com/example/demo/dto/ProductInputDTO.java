package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Schema(description = "Продукт(входные данные)")
@Data
public class ProductInputDTO {

    @NotBlank
    @Schema(description = "Название")
    private String title;

    @Positive
    @Schema(description = "Цена")
    private BigDecimal price;

    @Schema(description = "ID продавца")
    private Integer sellerId;

}
