package com.example.demo.dto;

import com.example.demo.validation.ValidDate;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.List;

@Schema(description = "Заказ(входные данные)")
@Data
public class OrderInputDTO {

    @Schema(description = "ID владельца заказа")
    private Integer customerId;

    @ValidDate
    @Schema(description = "Дата открытия")
    private LocalDate openDate;

    @ValidDate
    @Schema(description = "Дата закрытия")
    private LocalDate closeDate;

    @NotEmpty
    @Schema(description = "Список продуктов")
    private List<Integer> productIds;
}
