package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Schema(description = "Продавец(входные данные)")
@Data
public class SellerInputDTO {

    @NotBlank
    @Schema(description = "Название")
    private String title;

    @NotBlank
    @Schema(description = "Страна")
    private String country;

    @NotBlank
    @Schema(description = "Город")
    private String city;

    @Pattern(regexp = "\\d{11}")
    @Schema(description = "Номер телефона")
    private String phoneNumber;

}
