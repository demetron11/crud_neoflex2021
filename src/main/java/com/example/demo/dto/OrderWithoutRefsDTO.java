package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDate;

@Schema(description = "Заказ(выходные данные)")
@Data
public class OrderWithoutRefsDTO {

    @Schema(description = "Дата открытия")
    private LocalDate openDate;

    @Schema(description = "Дата закрытия")
    private LocalDate closeDate;

}
