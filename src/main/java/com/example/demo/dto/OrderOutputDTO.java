package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Schema(description = "Заказ(выходные данные)")
@Data
public class OrderOutputDTO {

    @Schema(description = "Дата открытия")
    private LocalDate openDate;

    @Schema(description = "Дата закрытия")
    private LocalDate closeDate;

    @Schema(description = "Владелец заказа")
    private CustomerInputDTO customer;

    @Schema(description = "Список продуктов")
    private List<ProductOutputDTO> products;

}
