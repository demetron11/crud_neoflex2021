package com.example.demo.dto;

import com.example.demo.validation.ValidDate;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Schema(description = "Пользователь(входные данные)")
@Data
public class CustomerInputDTO {

    @NotBlank
    @Schema(description = "Имя пользователя")
    private String name;

    @Pattern(regexp = "\\d{11}")
    @Schema(description = "Номер телефона")
    private String phoneNumber;

    @ValidDate
    @Schema(description = "Дата рождения")
    private LocalDate birthday;

    @NotBlank
    @Schema(description = "Адрес")
    private String address;
}
