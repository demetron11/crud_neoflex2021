package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Schema(description = "Пользователь(выходные данные)")
@Data
public class CustomerOutputDTO {

    @Schema(description = "Имя пользователя")
    private String name;

    @Schema(description = "Номер телефона")
    private String phoneNumber;

    @Schema(description = "Дата рождения")
    private LocalDate birthday;

    @Schema(description = "Адрес")
    private String address;

    @Schema(description = "Список заказов")
    private List<OrderWithoutRefsDTO> orders;
}
