package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;

@Schema(description = "Продукт(выходные данные)")
@Data
public class ProductOutputDTO {

    @Schema(description = "Название")
    private String title;

    @Schema(description = "Цена")
    private BigDecimal price;

}
