package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Schema(description = "Продавец(выходные данные)")
@Data
public class SellerOutputDTO {

    @Schema(description = "Название")
    private String title;

    @Schema(description = "Страна")
    private String country;

    @Schema(description = "Город")
    private String city;

    @Schema(description = "Номер телефона")
    private String phoneNumber;

    @Schema(description = "Список продуктов")
    private List<ProductOutputDTO> products;
}
