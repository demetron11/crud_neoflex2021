package com.example.demo.controller;

import com.example.demo.dto.CustomerInputDTO;
import com.example.demo.dto.CustomerOutputDTO;
import com.example.demo.entity.Customer;
import com.example.demo.mapper.CustomerMapper;
import com.example.demo.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Пользователь")
@AllArgsConstructor
@RestController
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerMapper customerMapper;

    @Operation(summary = "Получить список всех пользователей")
    @GetMapping
    public ResponseEntity<List<CustomerOutputDTO>> getAllCustomers() {
        return new ResponseEntity<>(customerService.getAllCustomers()
                .stream()
                .map(customerMapper::mapToDTO)
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @Operation(summary = "Получить пользователя")
    @GetMapping("/{id}")
    public ResponseEntity<CustomerOutputDTO> getCustomerById(@PathVariable @Parameter(description = "Идентификатор пользователя") Integer id) {
        return new ResponseEntity<>(customerMapper.mapToDTO(customerService.getCustomerById(id)), HttpStatus.OK);
    }

    @Operation(summary = "Добавить пользователя")
    @PostMapping
    public ResponseEntity<Integer> createCustomer(@RequestBody @Parameter(description = "Тело запроса") @Valid CustomerInputDTO customerInputDTO) {
        return new ResponseEntity<>(customerService.saveCustomer(customerMapper.mapToCustomer(customerInputDTO)), HttpStatus.CREATED);
    }

    @Operation(summary = "Удалить пользователя")
    @DeleteMapping("/{id}")
    public ResponseEntity<Customer> deleteCustomerById(@PathVariable @Parameter(description = "Идентификатор пользователя") Integer id) {
        customerService.deleteCustomerById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Редактировать пользователя")
    @PutMapping("/{id}")
    public ResponseEntity<Customer> updateCustomer(@PathVariable @Parameter(description = "Идентификатор пользователя") Integer id,
                                                   @RequestBody @Parameter(description = "Тело запроса") @Valid CustomerInputDTO customerInputDTO) {
        customerService.updateCustomer(id, customerMapper.mapToCustomer(customerInputDTO));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
