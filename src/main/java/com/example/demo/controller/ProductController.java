package com.example.demo.controller;

import com.example.demo.dto.ProductInputDTO;
import com.example.demo.dto.ProductOutputDTO;
import com.example.demo.entity.Product;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Продукт")
@AllArgsConstructor
@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;
    private final ProductMapper productMapper;

    @Operation(summary = "Получить список всех продуктов")
    @GetMapping
    public ResponseEntity<List<ProductOutputDTO>> getAllProducts() {
        return new ResponseEntity<>(productService.getAllProducts()
                .stream()
                .map(productMapper::mapToDTO)
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @Operation(summary = "Получить продукт")
    @GetMapping("/{id}")
    public ResponseEntity<ProductOutputDTO> getProductById(@PathVariable @Parameter(description = "Идентификатор продукта") Integer id) {
        return new ResponseEntity<>(productMapper.mapToDTO(productService.getProductById(id)), HttpStatus.OK);
    }

    @Operation(summary = "Добавить продукт")
    @PostMapping
    public ResponseEntity<Integer> createProduct(@RequestBody @Parameter(description = "Тело запроса") @Valid ProductInputDTO productInputDTO) {
        return new ResponseEntity<>(productService.saveProduct(productMapper.mapToProduct(productInputDTO)), HttpStatus.CREATED);
    }

    @Operation(summary = "Удалить продукт")
    @DeleteMapping("/{id}")
    public ResponseEntity<Product> deleteProductById(@PathVariable @Parameter(description = "Идентификатор продукта") Integer id) {
        productService.deleteProductById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Редактировать продукт")
    @PutMapping("/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable @Parameter(description = "Идентификатор продукта") Integer id,
                                                   @RequestBody @Parameter(description = "Тело запроса") @Valid ProductInputDTO productInputDTO) {
        productService.updateProduct(id, productMapper.mapToProduct(productInputDTO));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
