package com.example.demo.controller;

import com.example.demo.dto.SellerInputDTO;
import com.example.demo.dto.SellerOutputDTO;
import com.example.demo.entity.Seller;
import com.example.demo.mapper.SellerMapper;
import com.example.demo.service.SellerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Продавец")
@AllArgsConstructor
@RestController
@RequestMapping("/sellers")
public class SellerController {

    private final SellerService sellerService;
    private final SellerMapper sellerMapper;

    @Operation(summary = "Получить список всех продавцов")
    @GetMapping
    public ResponseEntity<List<SellerOutputDTO>> getAllSellers() {
        return new ResponseEntity<>(sellerService.getAllSellers()
                .stream()
                .map(sellerMapper::mapToDTO)
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @Operation(summary = "Получить продавца")
    @GetMapping("/{id}")
    public ResponseEntity<SellerOutputDTO> getSellerById(@PathVariable @Parameter(description = "Идентификатор продавца") Integer id) {
        return new ResponseEntity<>(sellerMapper.mapToDTO(sellerService.getSellerById(id)), HttpStatus.OK);
    }

    @Operation(summary = "Добавить продавца")
    @PostMapping
    public ResponseEntity<Integer> createSeller(@RequestBody @Parameter(description = "Тело запроса") @Valid SellerInputDTO sellerInputDTO) {
        return new ResponseEntity<>(sellerService.saveSeller(sellerMapper.mapToSeller(sellerInputDTO)), HttpStatus.CREATED);
    }

    @Operation(summary = "Удалить продавца")
    @DeleteMapping("/{id}")
    public ResponseEntity<Seller> deleteSellerById(@PathVariable @Parameter(description = "Идентификатор продавца") Integer id) {
        sellerService.deleteSellerById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Редактировать продавца")
    @PutMapping("/{id}")
    public ResponseEntity<Seller> updateSeller(@PathVariable @Parameter(description = "Идентификатор продавца") Integer id,
                                                   @RequestBody @Parameter(description = "Тело запроса") @Valid SellerInputDTO sellerInputDTO) {
        sellerService.updateSeller(id, sellerMapper.mapToSeller(sellerInputDTO));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
