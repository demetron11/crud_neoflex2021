package com.example.demo.controller;

import com.example.demo.dto.OrderInputDTO;
import com.example.demo.dto.OrderOutputDTO;
import com.example.demo.entity.Order;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Заказ")
@AllArgsConstructor
@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;
    private final OrderMapper orderMapper;

    @Operation(summary = "Получить список всех заказов")
    @GetMapping
    public ResponseEntity<List<OrderOutputDTO>> getAllOrders() {
        return new ResponseEntity<>(orderService.getAllOrders()
                .stream()
                .map(orderMapper::mapToDTO)
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @Operation(summary = "Получить заказ")
    @GetMapping("/{id}")
    public ResponseEntity<OrderOutputDTO> getOrderById(@PathVariable @Parameter(description = "Идентификатор заказа") Integer id) {
        return new ResponseEntity<>(orderMapper.mapToDTO(orderService.getOrderById(id)), HttpStatus.OK);
    }

    @Operation(summary = "Добавить заказ")
    @PostMapping
    public ResponseEntity<Integer> createOrder(@RequestBody @Parameter(description = "Тело запроса") @Valid OrderInputDTO orderInputDTO) {
        return new ResponseEntity<>(orderService.saveOrder(orderMapper.mapToOrder(orderInputDTO)), HttpStatus.CREATED);
    }

    @Operation(summary = "Удалить заказ")
    @DeleteMapping("/{id}")
    public ResponseEntity<Order> deleteOrderById(@PathVariable @Parameter(description = "Идентификатор заказа") Integer id) {
        orderService.deleteOrderById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Редактировать заказ")
    @PutMapping("/{id}")
    public ResponseEntity<Order> updateOrder(@PathVariable @Parameter(description = "Идентификатор заказа") Integer id,
                                                   @RequestBody @Parameter(description = "Тело запроса") @Valid OrderInputDTO orderInputDTO) {
        orderService.updateOrder(id, orderMapper.mapToOrder(orderInputDTO));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
