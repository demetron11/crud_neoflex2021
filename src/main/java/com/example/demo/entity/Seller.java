package com.example.demo.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Schema(description = "Продавец")
@Table(name = "sellers")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Seller {

    @Schema(description = "ID продавца")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Schema(description = "Название")
    @Column(length = 50)
    private String title;

    @Schema(description = "Страна")
    @Column(length = 50)
    private String country;

    @Schema(description = "Город")
    @Column(length = 50)
    private String city;

    @Schema(description = "Номер телефона")
    @Column(name = "phone_number", length = 11)
    private String phoneNumber;

    @Schema(description = "Список продуктов")
    @OneToMany(mappedBy = "seller")
    private List<Product> products;

}