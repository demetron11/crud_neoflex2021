package com.example.demo.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Schema(description = "Заказ")
@Table(name = "orders")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Order {

    @Schema(description = "ID заказа")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Schema(description = "ID владельца заказа")
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Schema(description = "Дата открытия")
    @Column(name = "open_date")
    private LocalDate openDate;

    @Schema(description = "Дата закрытия")
    @Column(name = "close_date")
    private LocalDate closeDate;

    @Schema(description = "Список продуктов")
    @ManyToMany
    @JoinTable(name = "orders_products",
            joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"))
    private List<Product> products;

}