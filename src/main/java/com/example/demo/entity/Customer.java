package com.example.demo.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;


@Schema(description = "Пользователь")
@Table(name = "customers")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Customer {

    @Schema(description = "ID пользователя")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Schema(description = "Имя пользователя")
    @Column(length = 50)
    private String name;

    @Schema(description = "Номер телефона")
    @Column(name = "phone_number", length = 11)
    private String phoneNumber;

    @Schema(description = "Дата рождения")
    @Column
    private LocalDate birthday;

    @Schema(description = "Адрес")
    @Column(length = 1000)
    private String address;

    @Schema(description = "Список заказов")
    @OneToMany(mappedBy = "customer")
    private List<Order> orders;

}