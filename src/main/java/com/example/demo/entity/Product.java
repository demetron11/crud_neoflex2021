package com.example.demo.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Schema(description = "Продукт")
@Table(name = "products")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @Schema(description = "ID продукта")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Schema(description = "Название")
    @Column(length = 50)
    private String title;

    @Schema(description = "Цена")
    @Column(precision = 8, scale = 2)
    private BigDecimal price;

    @Schema(description = "ID продавца")
    @ManyToOne
    @JoinColumn(name = "seller_id")
    private Seller seller;

    @Schema(description = "Список заказов")
    @ManyToMany(mappedBy = "products")
    private List<Order> orders;

}